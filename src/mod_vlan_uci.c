/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2021 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <stdio.h>
#include <string.h>

#include <debug/sahtrace.h>
#include <debug/sahtrace_macros.h>

#include <amxc/amxc.h>
#include <amxc/amxc_macros.h>
#include <amxp/amxp.h>

#include <amxm/amxm.h>

#include "mod_vlan_uci.h"
#include "mod_vlan_uci_util.h"

#define DEVICE_SECTION "dev"
#define INTERFACE_SECTION "intf"

static int add_device(const char* name, const char* ifname, int32_t vid, amxc_var_t* ret) {
    int rv = -1;
    amxc_var_t uci_args;
    amxc_var_t* values = NULL;
    amxc_string_t section_name;
    amxc_var_t* dev_ret = NULL;
    amxc_var_t* tmp = NULL;

    amxc_var_init(&uci_args);
    amxc_var_set_type(&uci_args, AMXC_VAR_ID_HTABLE);
    amxc_string_init(&section_name, 0);
    amxc_string_setf(&section_name, "%s_dev", name);

    amxc_var_add_key(cstring_t, &uci_args, "config", NETWORK_CONFIG);
    amxc_var_add_key(cstring_t, &uci_args, "type", "device");
    tmp = amxc_var_add_new_key(&uci_args, "name");
    amxc_var_push(cstring_t, tmp, amxc_string_take_buffer(&section_name));
    values = amxc_var_add_key(amxc_htable_t, &uci_args, "values", NULL);
    amxc_var_add_key(cstring_t, values, "type", "8021q");
    amxc_var_add_key(cstring_t, values, "name", name);
    amxc_var_add_key(cstring_t, values, "ifname", ifname);
    amxc_var_add_key(int32_t, values, "vid", vid);

    dev_ret = amxc_var_add_key(amxc_htable_t, ret, "device", NULL);
    rv = uci_call("add", &uci_args, dev_ret);

    amxc_string_clean(&section_name);
    amxc_var_clean(&uci_args);
    return rv;
}

static int add_interface(const char* name, amxc_var_t* ret) {
    int rv = -1;
    amxc_var_t uci_args;
    amxc_var_t* values = NULL;
    amxc_string_t section_name;
    amxc_var_t* intf_ret = NULL;
    amxc_var_t* tmp = NULL;

    amxc_var_init(&uci_args);
    amxc_var_set_type(&uci_args, AMXC_VAR_ID_HTABLE);
    amxc_string_init(&section_name, 0);
    amxc_string_setf(&section_name, "%s_intf", name);

    amxc_var_add_key(cstring_t, &uci_args, "config", NETWORK_CONFIG);
    amxc_var_add_key(cstring_t, &uci_args, "type", "interface");
    tmp = amxc_var_add_new_key(&uci_args, "name");
    amxc_var_push(cstring_t, tmp, amxc_string_take_buffer(&section_name));
    values = amxc_var_add_key(amxc_htable_t, &uci_args, "values", NULL);
    amxc_var_add_key(cstring_t, values, "ifname", name);

    intf_ret = amxc_var_add_key(amxc_htable_t, ret, "interface", NULL);
    rv = uci_call("add", &uci_args, intf_ret);

    amxc_string_clean(&section_name);
    amxc_var_clean(&uci_args);
    return rv;
}

static int delete_section(const char* section, const char* type, amxc_var_t* ret) {
    int rv = -1;
    amxc_string_t section_name;
    amxc_var_t uci_args;
    amxc_var_t* tmp = NULL;

    amxc_string_init(&section_name, 0);
    amxc_string_setf(&section_name, "%s_%s", section, type);
    amxc_var_init(&uci_args);
    amxc_var_set_type(&uci_args, AMXC_VAR_ID_HTABLE);

    amxc_var_add_key(cstring_t, &uci_args, "config", NETWORK_CONFIG);
    tmp = amxc_var_add_new_key(&uci_args, "section");
    amxc_var_push(cstring_t, tmp, amxc_string_take_buffer(&section_name));

    rv = uci_call("delete", &uci_args, ret);

    amxc_string_clean(&section_name);
    amxc_var_clean(&uci_args);
    return rv;
}

static int vlan_setprio(UNUSED amxc_var_t* args, const char* name) {
    (void) name; // can not be set to unused when traces are turned on.
    SAH_TRACEZ_INFO(ME, "could not set %s priority, not supported by this module", name);
    return 0;
}

static int create_vlan(UNUSED const char* function_name,
                       amxc_var_t* args,
                       amxc_var_t* ret) {
    int rv = -1;
    const char* ifname = GETP_CHAR(args, "port_name");
    const char* name = GETP_CHAR(args, "vlanport_name");
    int32_t vid = GETP_INT32(args, "vlanid");
    amxc_string_t default_name;

    amxc_string_init(&default_name, 0);

    //name can be empty, a default name will be used
    when_str_empty_trace(ifname, exit, ERROR, "no lowerlayer interface, no port configured");
    when_false_trace(vid > 0, exit, ERROR, "VLANID needs to be greater than 0");

    SAH_TRACEZ_INFO(ME, "create vlan(name=%s llintf=%s vlanid=%d)", name, ifname, vid);

    if((name == NULL) || (*name == 0)) {
        amxc_string_setf(&default_name, "%s.%d", ifname, vid);
        name = amxc_string_get(&default_name, 0);
    }
    amxc_var_set_type(ret, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, ret, "vlanport_name", name);

    rv = add_device(name, ifname, vid, ret);
    when_failed_trace(rv, exit, ERROR, "failed to add device section for %s", name);
    rv = add_interface(name, ret);
    when_failed_trace(rv, exit, ERROR, "failed to add interface section for %s", name);

    rv = vlan_setprio(args, name);

exit:
    amxc_string_clean(&default_name);
    return rv;
}

static int destroy_vlan(UNUSED const char* function_name,
                        amxc_var_t* args,
                        UNUSED amxc_var_t* ret) {
    int rv = -1;
    const char* name = GETP_CHAR(args, "vlanport_name");

    when_str_empty_trace(name, exit, ERROR, "can not destroy vlan, no name given");
    SAH_TRACEZ_INFO(ME, "vlan unregister(name=%s)", name);

    rv = delete_section(name, DEVICE_SECTION, ret);
    when_failed_trace(rv, exit, ERROR, "failed to remove device section for %s", name);

    rv = delete_section(name, INTERFACE_SECTION, ret);
    when_failed_trace(rv, exit, ERROR, "failed to remove interface section for %s", name);

exit:
    return rv;
}

static int set_vlan_prio(UNUSED const char* function_name,
                         amxc_var_t* args,
                         UNUSED amxc_var_t* ret) {
    int rv = -1;
    const char* name = GETP_CHAR(args, "vlanport_name");
    when_str_empty_trace(name, exit, ERROR, "can not set priority, no vlan name provided");
    rv = vlan_setprio(args, name);

exit:
    return rv;
}

static AMXM_CONSTRUCTOR vlan_start(void) {
    amxm_shared_object_t* so = amxm_so_get_current();
    amxm_module_t* mod = NULL;

    amxm_module_register(&mod, so, MOD_VLAN_CTRL);
    amxm_module_add_function(mod, "create-vlan", create_vlan);
    amxm_module_add_function(mod, "destroy-vlan", destroy_vlan);
    amxm_module_add_function(mod, "set-vlan-priority", set_vlan_prio);

    return uci_commit_init();
}

static AMXM_DESTRUCTOR vlan_stop(void) {

    uci_commit_clean();
    return 0;
}
