# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]


## Release v0.1.4 - 2023-05-10(08:02:02 +0000)

### Other

- [Coverage] Remove SAHTRACE defines in order to increase branching coverage

## Release v0.1.3 - 2023-03-20(09:47:57 +0000)

### Changes

-  tr181-components: missing explicit dependency on rpcd service providing ubus uci backend

## Release v0.1.2 - 2023-01-09(10:50:20 +0000)

### Fixes

- avoidable copies of strings, htables and lists

## Release v0.1.1 - 2021-11-04(11:53:23 +0000)

### Changes

- Move vlan modules from ambiorix folder to core on gitlab.com

## Release v0.1.0 - 2021-10-28(09:13:03 +0000)

### New

- Create a uci vlan module

